============
General Info
============

========= ========================= ===========================================
Parameter Value                     Description
========= ========================= ===========================================
MCC-MNC   238-42                    Mobile Country Code and Mobile Network Code
SMSC GT   4593702020                SMSC Global Title
Data APN  data.tel42.com            Access Point Name for mobile data
MMS APN   data.tel42.com            Access Point Name for MMS
MMS MMSC  http://mms.tel42.com:8000 MMSC URL for MMS
========= ========================= ===========================================

42 is roaming nationally with Telenor Denmark on MCC-MNC 238-66 and 238-02.