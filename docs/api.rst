===
API
===

General Info
============

The API is a HTTP RESTful API based around resource oriented URLs. It uses standard HTTP verbs and returns JSON on all requests.

Base URL
--------

All URLs referenced in the documentation have the following base:

::

  https://api.forty-two.com

The API is served over HTTPS. To ensure data privacy, unencrypted HTTP is not supported.

Authentication
--------------

HTTP requests to the API are protected with HTTP Basic authentication.

In short, you will use your API key as the username and your API secret as the password for HTTP Basic authentication.

Sample Call in NodeJS Request:

.. code-block:: javascript

  var request = require("request");

  var options = {
    method: "GET",
    url:    "/",
    headers:
      { Authorization: "Basic " + new Buffer(api_key + ":" + api_secret).toString("base64") }
  };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    console.log(body);
  });

Retrieve SIM
============

- Method

  ::

    GET

- URL

  ::

    /sim/:imsi

- URL Params

  ========= ========================
  Parameter Description
  ========= ========================
  imsi      15 digit IMSI of the SIM
  ========= ========================

- Success Response

  .. code-block:: javascript

    Code: 200
    Content: { "imsi":  "238421234567890",
               "iccid": "89123456789012345678",
               "ki":    "A1B2C3D4E5F6G7H8I9J0K1L2M3N4O5P6",
               "opc":   "6P5O4N3M2L1K0J9I8H7G6F5E4D3C2B1A",
               "pin1":  "1234",
               "pin2":  "4321",
               "puk1":  "12345678",
               "puk2":  "87654321",
               "adm1":  "A1B2C3D4E5F6G7H8" }

- Error Codes

  ==== ==============
  Code Error
  ==== ==============
  400  imsi_invalid
  403  imsi_forbidden
  404  imsi_not_found
  ==== ==============

- Sample Call in NodeJS Request

  .. code-block:: javascript

    var request = require("request");

    var options = {
      method: "GET",
      url:    "/sim/238421234567890",
      headers:
        { Authorization: "Basic " + new Buffer(api_key + ":" + api_secret).toString("base64") }
    };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      console.log(body);
    });

Create SIM
==========

- Method

  ::

    POST

- URL

  ::

    /sim/:imsi

- URL Params

  ========= ========================
  Parameter Description
  ========= ========================
  imsi      15 digit IMSI of the SIM
  ========= ========================

- Data Params

  ========= ======== =========================
  Parameter Required Description
  ========= ======== =========================
  iccid     *        20 digit ICCID of the SIM
  ki        *        32 alphanumeric string
  opc       *        32 alphanumeric string
  pin1      *        4 digit code
  pin2      *        4 digit code
  puk1      *        8 digit code
  puk2      *        8 digit code
  adm1      *        16 alphanumeric string
  ========= ======== =========================

- Success Response

  .. code-block:: javascript

    Code: 200
    Content: { "message": "OK" }

- Error Codes

  ==== ==============
  Code Error
  ==== ==============
  400  imsi_invalid
  400  iccid_invalid
  400  ki_invalid
  400  opc_invalid
  400  pin1_invalid
  400  pin2_invalid
  400  puk1_invalid
  400  puk2_invalid
  400  adm1_invalid
  403  imsi_forbidden
  409  imsi_conflict
  ==== ==============

- Sample Call in NodeJS Request

  .. code-block:: javascript

    var request = require("request");

    var options = {
      method: "POST",
      url:    "/sim/238421234567890",
      headers:
        { Authorization:  "Basic " + new Buffer(api_key + ":" + api_secret).toString("base64"),
          "content-type": "application/x-www-form-urlencoded" },
      form:
        { iccid: "89123456789012345678",
          ki:    "A1B2C3D4E5F6G7H8I9J0K1L2M3N4O5P6",
          opc:   "6P5O4N3M2L1K0J9I8H7G6F5E4D3C2B1A",
          pin1:  "1234",
          pin2:  "4321",
          puk1:  "12345678",
          puk2:  "87654321",
          adm1:  "A1B2C3D4E5F6G7H8" }
    };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      console.log(body);
    });

Update SIM
==========

- Method

  ::

    PUT

- URL

  ::

    /sim/:imsi

- URL Params

  ========= ========================
  Parameter Description
  ========= ========================
  imsi      15 digit IMSI of the SIM
  ========= ========================

- Data Params

  ========= =========================
  Parameter Description
  ========= =========================
  iccid     20 digit ICCID of the SIM
  ki        32 alphanumeric string
  opc       32 alphanumeric string
  pin1      4 digit code
  pin2      4 digit code
  puk1      8 digit code
  puk2      8 digit code
  adm1      16 alphanumeric string
  ========= =========================

- Success Response

  .. code-block:: javascript

    Code: 200
    Content: { "message": "OK" }

- Error Codes

  ==== ==============
  Code Error
  ==== ==============
  400  imsi_invalid
  400  iccid_invalid
  400  ki_invalid
  400  opc_invalid
  400  pin1_invalid
  400  pin2_invalid
  400  puk1_invalid
  400  puk2_invalid
  400  adm1_invalid
  403  imsi_forbidden
  404  imsi_not_found
  ==== ==============

- Sample Call in NodeJS Request

  .. code-block:: javascript

    var request = require("request");

    var options = {
      method: "PUT",
      url:    "/sim/238421234567890",
      headers:
        { Authorization:  "Basic " + new Buffer(api_key + ":" + api_secret).toString("base64"),
          "content-type": "application/x-www-form-urlencoded" },
      form:
        { iccid: "89123456789012345678",
          ki:    "A1B2C3D4E5F6G7H8I9J0K1L2M3N4O5P6",
          opc:   "6P5O4N3M2L1K0J9I8H7G6F5E4D3C2B1A",
          pin1:  "1234",
          pin2:  "4321",
          puk1:  "12345678",
          puk2:  "87654321",
          adm1:  "A1B2C3D4E5F6G7H8" }
    };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      console.log(body);
    });

Delete SIM
==========

- Method

  ::

    DELETE

- URL

  ::

    /sim/:imsi

- URL Params

  ========= ========================
  Parameter Description
  ========= ========================
  imsi      15 digit IMSI of the SIM
  ========= ========================

- Success Response

  .. code-block:: javascript

    Code: 200
    Content: { "message": "OK" }

- Error Codes

  ==== ==============================
  Code Error
  ==== ==============================
  400  imsi_invalid
  403  imsi_forbidden
  404  imsi_not_found
  409  imsi_subscriber_mapping_exists
  ==== ==============================

- Sample Call in NodeJS Request

  .. code-block:: javascript

    var request = require("request");

    var options = {
      method: "DELETE",
      url:    "/sim/238421234567890",
      headers:
        { Authorization: "Basic " + new Buffer(api_key + ":" + api_secret).toString("base64") }
    };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      console.log(body);
    });

Retrieve Subscriber
===================

- Method

  ::

    GET

- URL

  ::

    /subscriber/:imsi

  OR

  ::

    /subscriber/:msisdn

- URL Params

  ========= ========================
  Parameter Description
  ========= ========================
  imsi      15 digit IMSI of the SIM
  ========= ========================

  OR

  ========= ==========================================
  Parameter Description
  ========= ==========================================
  msisdn    Phone number in international E.164 format
  ========= ==========================================

- Success Response

  .. code-block:: javascript

    Code: 200
    Content: { "imsi":          "238421234567890",
               "msisdn":        "4512345678",
               "cs":            true,
               "ps":            true,
               "eps":           true,
               "cs_roaming":    true,
               "ps_roaming":    true,
               "ps_throttling": false }

- Error Codes

  ==== ====================
  Code Error
  ==== ====================
  400  imsi_invalid
  400  msisdn_invalid
  403  imsi_forbidden
  404  subscriber_not_found
  ==== ====================

- Sample Call in NodeJS Request

  .. code-block:: javascript

    var request = require("request");

    var options = {
      method: "GET",
      url:    "/subscriber/4512345678",
      headers:
        { Authorization: "Basic " + new Buffer(api_key + ":" + api_secret).toString("base64") }
    };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      console.log(body);
    });

Create Subscriber
=================

- Method

  ::

    POST

- URL

  ::

    /subscriber/:imsi

  OR

  ::

    /subscriber/:msisdn

- URL Params

  ========= ========================
  Parameter Description
  ========= ========================
  imsi      15 digit IMSI of the SIM
  ========= ========================

  OR

  ========= ==========================================
  Parameter Description
  ========= ==========================================
  msisdn    Phone number in international E.164 format
  ========= ==========================================

- Data Params

  ============= ============= ======= ================================================================
  Parameter     Required      Default Description
  ============= ============= ======= ================================================================
  imsi          * (or msisdn)         15 digit IMSI of the SIM
  msisdn        * (or imsi)           Phone number in international E.164 format
  cs                          true    Boolean to enable/disable circuit switching (calls/SMS over GSM)
  ps                          true    Boolean to enable/disable packet switching (data over GPRS/EDGE)
  eps                         true    Boolean to enable/disable evolved packet system (data over LTE)
  cs_roaming                  false   Boolean to enable/disable circuit switching roaming
  ps_roaming                  false   Boolean to enable/disable packet switching roaming
  ps_throttling               false   Boolean to enable/disable packet switching bandwidth throttling
  ============= ============= ======= ================================================================

- Success Response

  .. code-block:: javascript

    Code: 200
    Content: { "message": "OK" }

- Error Codes

  ==== =========================
  Code Error
  ==== =========================
  400  imsi_invalid
  400  msisdn_invalid
  400  cs_invalid
  400  ps_invalid
  400  eps_invalid
  400  cs_roaming_invalid
  400  ps_roaming_invalid
  400  ps_throttling_invalid
  403  imsi_forbidden
  404  imsi_not_found
  409  imsi_conflict
  409  ps_ps_throttling_conflict
  ==== =========================

- Sample Call in NodeJS Request

  .. code-block:: javascript

    var request = require("request");

    var options = {
      method: "POST",
      url:    "/subscriber/4512345678",
      headers:
        { Authorization:  "Basic " + new Buffer(api_key + ":" + api_secret).toString("base64"),
          "content-type": "application/x-www-form-urlencoded" },
      form:
        { imsi:          "238421234567890",
          cs:            true,
          ps:            true,
          eps:           true,
          cs_roaming:    true,
          ps_roaming:    true,
          ps_throttling: false }
    };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      console.log(body);
    });

Update Subscriber
=================

- Method

  ::

    PUT

- URL

  ::

    /subscriber/:imsi

  OR

  ::

    /subscriber/:msisdn

- URL Params

  ========= ========================
  Parameter Description
  ========= ========================
  imsi      15 digit IMSI of the SIM
  ========= ========================

  OR

  ========= ==========================================
  Parameter Description
  ========= ==========================================
  msisdn    Phone number in international E.164 format
  ========= ==========================================

- Data Params

  ============= ================================================================
  Parameter     Description
  ============= ================================================================
  imsi          15 digit IMSI of the SIM
  msisdn        Phone number in international E.164 format
  cs            Boolean to enable/disable circuit switching (calls/SMS over GSM)
  ps            Boolean to enable/disable packet switching (data over GPRS/EDGE)
  eps           Boolean to enable/disable evolved packet system (data over LTE)
  cs_roaming    Boolean to enable/disable circuit switching roaming
  ps_roaming    Boolean to enable/disable packet switching roaming
  ps_throttling Boolean to enable/disable packet switching bandwidth throttling
  ============= ================================================================

- Success Response

  .. code-block:: javascript

    Code: 200
    Content: { "message": "OK" }

- Error Codes

  ==== =========================
  Code Error
  ==== =========================
  400  imsi_invalid
  400  msisdn_invalid
  400  cs_invalid
  400  ps_invalid
  400  eps_invalid
  400  cs_roaming_invalid
  400  ps_roaming_invalid
  400  ps_throttling_invalid
  403  imsi_forbidden
  404  subscriber_not_found
  409  ps_ps_throttling_conflict
  ==== =========================

- Sample Call in NodeJS Request

  .. code-block:: javascript

    var request = require("request");

    var options = {
      method: "PUT",
      url:    "/subscriber/4512345678",
      headers:
        { Authorization:  "Basic " + new Buffer(api_key + ":" + api_secret).toString("base64"),
          "content-type": "application/x-www-form-urlencoded" },
      form:
        { imsi:          "238421234567890",
          cs:            true,
          ps:            true,
          eps:           true,
          cs_roaming:    true,
          ps_roaming:    true,
          ps_throttling: false }
    };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      console.log(body);
    });

Delete Subscriber
=================

- Method

  ::

    DELETE

- URL

  ::

    /subscriber/:imsi

  OR

  ::

    /subscriber/:msisdn

- URL Params

  ========= ========================
  Parameter Description
  ========= ========================
  imsi      15 digit IMSI of the SIM
  ========= ========================

  OR

  ========= ==========================================
  Parameter Description
  ========= ==========================================
  msisdn    Phone number in international E.164 format
  ========= ==========================================

- Success Response

  .. code-block:: javascript

    Code: 200
    Content: { "message": "OK" }

- Error Codes

  ==== ====================
  Code Error
  ==== ====================
  400  imsi_invalid
  400  msisdn_invalid
  403  imsi_forbidden
  404  subscriber_not_found
  ==== ====================

- Sample Call in NodeJS Request

  .. code-block:: javascript

    var request = require("request");

    var options = {
      method: "DELETE",
      url:    "/subscriber/4512345678",
      headers:
        { Authorization: "Basic " + new Buffer(api_key + ":" + api_secret).toString("base64") }
    };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      console.log(body);
    });

Send SMS
========

- Method

  ::

    POST

- URL

  ::

    /sms/:to

- URL Params

  ========= ==========================================
  Parameter Description
  ========= ==========================================
  to        Phone number in international E.164 format
  ========= ==========================================

- Data Params

  ========= ======================
  Parameter Description
  ========= ======================
  from      An alphanumeric string
  text      Text in UTF-8 encoding
  ========= ======================

- Success Response

  .. code-block:: javascript

    Code: 200
    Content: { "message": "OK" }

- Error Codes

  ==== ============
  Code Error
  ==== ============
  400  to_invalid
  400  from_invalid
  400  text_invalid
  ==== ============

- Sample Call in NodeJS Request

  .. code-block:: javascript

    var request = require("request");

    var options = {
      method: "POST",
      url:    "/sms/4512345678",
      headers:
        { Authorization:  "Basic " + new Buffer(api_key + ":" + api_secret).toString("base64"),
          "content-type": "application/x-www-form-urlencoded" },
      form:
        { from: "42",
          text: "Hello World" }
    };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      console.log(body);
    });
